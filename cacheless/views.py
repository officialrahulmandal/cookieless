# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
# Create your views here.
def home(request):
    return render(request,"cacheless/homepage.html")


def cookieless(request):
    response = HttpResponseRedirect("http://127.0.0.1:8000/cookie/")
    response.delete_cookie('csrftoken')
    return response
